
const addCommentCM = document.getElementById("addCommentCM");
const saveJsonCM = document.getElementById("saveJsonCM");
const saveHtmlCM = document.getElementById("saveHtmlCM");
const resultCM = document.getElementById("resultCM");
const textCM = document.getElementById("textCM");
const clearCM = document.getElementById("clearCM");
const pasteCM = document.getElementById("pasteCM");
const copyCM = document.getElementById("copyCM");

const displayMenu = document.getElementById("displayMenu");
const headerMenu = document.getElementById("headerMenu");
const textMenu = document.getElementById("textMenu");
const linkMenu = document.getElementById("linkMenu");

const resultArea = document.getElementById("resultArea");
const textArea = document.getElementById("textArea");
const linkTitle = document.getElementById("linkTitle");
const linkButton = document.getElementById("linkButton");
const dropzone = document.getElementById("dropzone");
const dropzoneText = document.getElementById("dropzoneText");
const popup = document.getElementById("popup");

var currentTextOption = document.getElementById("p");
var currentHeaderOption = document.getElementById("h1");
var currentDisplayOption = document.getElementById("json");

var caretPosition = 0;
var headerClicked = false;
var linkClicked = false;
var listClicked = false;
var textCMClicked = false;
var resultCMClicked = false;

var json = [];
var selectedText = "";
var capturedText = "";
var isFirefox = false;
var isBlink = false;
var storage;

class Block {
    constructor(block, content, modifier) {
        this.block = block;
        this.content = content;
        this.modifier = modifier;
    }
}

class Modifier {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

window.addEventListener("dragenter", function (event) {
    dropzoneText.innerText = "Umieść tu plik HTML lub JSON";
    dropzone.style.display = "block";
});

window.addEventListener("dragleave", function (event) {
    if (event.target === dropzone || event.target === document) {
        dropzone.style.display = "none";
    }
});

document.body.onload = function () {
    var userAgent = navigator.userAgent;
    isFirefox = /Firefox/.test(userAgent);
    isBlink = /Chrome/.test(userAgent);
};

document.onclick = function () {
    if (textCMClicked) {
        textCM.style.display = "none";
    }

    if (resultCMClicked) {
        resultCM.style.display = "none";
    }
};

dropzone.ondragover = function (event) {
    event.preventDefault();
};

dropzone.ondrop = function (event) {
    event.preventDefault();

    var item = event.dataTransfer.items[0];

    if (item.type === "text/html" || item.type === "application/json") {
        var file = item.getAsFile();
        readDataFromFile(file);
        dropzone.style.display = "none";

    } else {
        dropzoneText.innerText = "Niewłaściwy plik";
        setTimeout(function () {
            dropzone.style.display = "none";
        }, 2000);
    }
    removeDrogData(event);
};

function removeDrogData(e) {
    if (e.dataTransfer.items) {
        e.dataTransfer.items.clear();
    } else {
        e.dataTransfer.clearData();
    }
}

function readDataFromFile(file) {
    var reader = new FileReader();

    reader.onload = function () {
        var text = reader.result;
        if (file.type === "application/json") {
            var newJson = JSON.parse(text);
            json = [];

            for (var n in newJson) {
                for (var t in textOptions) {
                    if (newJson[n].block === textOptions[t]) {
                        json.push(newJson[n]);
                    }
                }
            }
            displayResult();

        } else {
            textArea.innerHTML = text;
            json = [];
            textToJson();
        }
    };
    reader.readAsText(file);
}

popup.onclick = function () {
    popup.style.visibility = "collapse";
};

function showPopup(statement = "UL") {
    
    if (storage === undefined) {
        storage = {"A": false, "UL": false};
    }

    if (storage[statement] === false) { 
        switch (statement) {
            case 'UL' : {
                popup.innerText = "Aby zakończyć edycje listy należy nacisnąć ponownie przycisk ENTER"
                storage["UL"] = true;
                break;
            }
            case 'A' : {
                popup.innerText = "Aby zakończyć edycje linku należy nacisnąć dwukrotnie przycisk SPACE"
                storage["A"] = true;
                break;
            }
        }
        popup.style.visibility = "visible";

        setTimeout(function() {
            popup.style.visibility = "collapse";
        }, 3000);
    }
}

textArea.onkeyup = function (event) {
    var keyCode = event.keyCode;

    if (keyCode > 36 && keyCode < 41) {
        saveCusrsorPosition();
        checkIsTextOptionChanged();

    }
    else {
        textToJson(keyCode);
        if (keyCode === 13) {
            closeHeaderMenu();
            closeLinkMenu();
        }
    }
};

textArea.onclick = function () {
    saveCusrsorPosition();
    checkIsTextOptionChanged();
    closeHeaderMenu();
    closeLinkMenu();
};

textArea.oncontextmenu = function (event) {
    saveCusrsorPosition();
    if (resultCMClicked) {
        resultCM.style.display = "none";
    }
    textCMClicked = true;
    textCM.style.display = "block";
    textCM.style.top = event.pageY + 'px';
    textCM.style.left = event.pageX + 'px';
    capturedText = document.getSelection().toString();
    return false;
};

copyCM.onclick = function () {
    selectedText = capturedText;
};

pasteCM.onclick = function () {
    var element = getCurrentBlock();

    if (element) {
        element.content += selectedText;
        displayResult();
    }
};

clearCM.onclick = function () {
    json = [];
    displayResult();
};

addCommentCM.onclick = function () {
    var line = countRealLline();
    var nextBlock;

    if (line.length > 0) {
        line = line[line.length - 1];
        nextBlock = json[line + 1];

        if (!nextBlock || (nextBlock && nextBlock.block !== "aside")) {

            var block = new Block("aside", "komentarz");
            json[line].modifier.push(new Modifier("class", "left"));
            json.splice(line + 1, 0, block);

            displayResult();
        }
    }
};

resultArea.oncontextmenu = function (event) {
    if (textCMClicked) {
        textCM.style.display = "none";
    }
    resultCMClicked = true;
    resultCM.style.display = "block";
    resultCM.style.top = event.pageY + 'px';
    resultCM.style.left = event.pageX + 'px';
    return false;
};

saveJsonCM.onclick = function () {
    var a = document.getElementById("downloadJson");
    var data = JSON.stringify(json, replacer, 2);
    var file = new Blob([data], { type: 'application/json' });
    a.href = URL.createObjectURL(file);
};

saveHtmlCM.onclick = function () {
    var html = jsonToHtml();
    var a = document.getElementById("downloadHtml");
    var file = new Blob([html], { type: 'text/html' });
    a.href = URL.createObjectURL(file);
};

textMenu.onclick = function (event) {
    var element = document.getElementById(event.target.id);
    var block;

    if (currentTextOption.id === 'a') {
        block = getCurrentBlock(false, true, true);

        if (currentTextOption.id === block.block) {
            textMenuClick(element);
        } else {

            if (!linkClicked) {
                linkClicked = !linkClicked;
            }
            linkMenu.style.display = linkClicked ? "block" : "none";
        }

    } else {
        textMenuClick(element);
    }
};

function textMenuClick(element) {
    var list = document.getElementById("ul");

    if (currentTextOption !== element) {
        changeTextOption(element);
        textToJson();

    } else if (currentTextOption === list) {
        listClicked = true;
        textToJson();
    }

    if (element.id === "header" || element.id === "section") {
        closeLinkMenu();
        headerClicked = !headerClicked;
        headerMenu.style.display = headerClicked ? "flex" : "none";

    } else if (element.id === "a" || element.id === "blockquote") {
        var text = element.id === "a" ? "URL: " : "Source: ";

        if (!linkClicked || linkTitle.innerText === text) {
            linkClicked = !linkClicked;
        } else
            closeHeaderMenu();

        linkTitle.innerText = text;
        linkMenu.style.display = linkClicked ? "block" : "none";

    } else {
        closeHeaderMenu();
        closeLinkMenu();
    }
}

headerMenu.onclick = function (event) {
    var element = document.getElementById(event.target.id);
    closeHeaderMenu();

    if (currentHeaderOption !== element) {
        changeHeaderOption(element);
        textToJson();
    }
};

linkButton.onclick = function (event) {
    var url = document.getElementById("linkUrl").value;

    if (url !== "") {
        closeLinkMenu();
        var element = getCurrentBlock(true);
        var modifier = element.modifier;
        var type = currentTextOption.id === 'a' ? "href" : "source";
        var isFound = false;

        for (var m = 0; m < modifier.length; m++) {
            if (modifier[m].name === type) {
                element.modifier[m].value = url;
                isFound = true;
            }
        }

        if (!isFound) {
            element.modifier.push(new Modifier(type, url));
        }

        displayResult();
    }
};

displayMenu.onclick = function (event) {
    var element = document.getElementById(event.target.id);

    if (currentDisplayOption !== element) {
        currentDisplayOption.style.color = "lightgrey";
        currentDisplayOption = element;
        currentDisplayOption.style.color = "black";
        resultArea.innerText = "";
        displayResult();
    }
};

function closeHeaderMenu() {
    if (headerClicked) {
        headerClicked = false;
        headerMenu.style.display = "none";
    }
}

function closeLinkMenu() {
    if (linkClicked) {
        linkClicked = false;
        linkMenu.style.display = "none";
    }
}

function changeTextOption(element) {
    currentTextOption.style.color = "lightgrey";
    currentTextOption = element;
    currentTextOption.style.color = "black";
}

function changeHeaderOption(element) {
    currentHeaderOption.style.color = "lightgrey";
    currentHeaderOption = element;
    currentHeaderOption.style.color = "black";
}

function changeTextOptionToP() {
    changeTextOption(document.getElementById('p'));
}

function countRealLline() {
    var data = getCaretData();
    var obj = countContentLines(json, data.line);
    return obj.list;
}

function countContentLines(cJson, line, list = []) {
    var content, obj;

    for (var j in cJson) {
        content = cJson[j].content;

        if (typeof content === "string") line--;

        else {
            for (var k in content) {
                if (content[k].content instanceof Array) {
                    obj = countContentLines(content[k].content, line, list);
                    line = obj.line;
                    list.concat(obj.list);

                } else line--;

                if (line < 0) {
                    list.push(k);
                    break;
                }
            }
        }

        if (line < 0) {
            list.push(j);
            break;
        }
    }
    return { line: line, list: list }
}

function checkIsTextOptionChanged() {

    if (json.length > 0) {

        var element;
        var block = getCurrentBlock(true);
        var reg = /h[1-6]/;

        block = block.block;
        block = block === "li" ? "ul" : block;

        if ((block !== currentTextOption.id || block !== currentHeaderOption.id) && block !== "aside") {

            if (reg.test(block)) {
                element = document.getElementById(block);
                changeHeaderOption(element);
                element = document.getElementById("header");

            } else {
                element = document.getElementById(block);
            }
            changeTextOption(element);
        }
    }
}

function processText(childNode, cJson, n, isTextTyped, section = false) {
    var name, innertext, text, number, children, child, childrenLen;
    var incraseLength = 0;

    name = childNode.localName;
    text = childNode.innerText;
    text = text[0] === '\n' || text === '' ? "<br>" : text;

    if (cJson[n]) {
        if (name === cJson[n].block) {
            children = childNode.children;

            if (children.length > 0 && children[0].localName !== "br") {

                childrenLen = children.length;

                if (childrenLen === 1 && children[0].localName === "a" && name !== "section") {
                    text = childNode.childNodes[0].nodeValue.replace(/\n|\t/g, "");
                    cJson[n].content[0].content = text;
                    innertext = children[0].innerText;
                    if (cJson[n].content[1].content.length < innertext.length || isBlink) {
                        showPopup("A");
                    }
                    cJson[n].content[1].content = innertext

                    if ((innertext[innertext.length - 2] === ' ' || innertext[innertext.length - 2] === '\xa0') && 
                    innertext[innertext.length - 1] === '\xa0' && cJson[n].content.length === 2) {        
                        
                        var lines = countRealLline();
                        var element = textArea;
                        
                        cJson[n].content[1].content = innertext.slice(0, -2);
                        name = cJson[n].block === "li" ? "ul" : cJson[n].block;
                        changeTextOption(document.getElementById(name));
                        cJson[n].content.push({ "content": "\xa0" });
                        
                        for (var l = lines.length - 1; l > 0; l--) {
                            element = element.children[lines[l]];
                        }
                        if (element.childNodes.length > 2) {
                            element.childNodes[2].nodeValue = " \n";
                        
                        } else {
                            element.appendChild(document.createTextNode(" \n"));
                        }
                        caretPosition--;
                        incraseLength += 100;
                    
                    } else if (isBlink && cJson[n].content.length === 2 && childNode.childNodes.length === 3 && childNode.childNodes[2].nodeValue[0] !== '\n') {
                        text = childNode.childNodes[2].nodeValue;
                        cJson[n].content[1].content += text;
                        incraseLength += 999;
                    
                    } else if (cJson[n].content.length === 3) {
                        if (childNode.childNodes.length > 2) {
                            text = childNode.childNodes[2].nodeValue.replace(/\n|\t/g, "");
                            if (text.length > 0) {
                                cJson[n].content[2].content = text;
                                if (isBlink) incraseLength += 999;
                            }
                        
                        } else {
                            cJson[n].content.pop();
                        }
                    }

                } else {
                    for (child = 0; child < children.length; child++) {
                        let pt = processText(children[child], cJson[n].content, child, isTextTyped, true);

                        if (pt && typeof pt === "boolean") {
                            cJson.push(new Block("p", "<br>", []));
                            incraseLength += 1;

                        } else {
                            childrenLen += pt;
                            if (pt > 99) incraseLength += pt;
                        }
                    }

                    if (cJson[n].content.length > childrenLen) {
                        while (childrenLen < cJson[n].content.length) {
                            cJson[n].content.pop();
                        }
                    }
                }

            } else {
                cJson[n].content = text;

                if (isFirefox && (name === "a" || name === "dfn" || name === "blockquote")) {

                    var childNodes = childNode.childNodes;
                    var lastChildNode = childNodes[childNodes.length - 1];

                    if (isFirefox && childNodes.length > 1 && childNodes[childNodes.length - 2].localName === "br") {

                        changeTextOptionToP();
                        cJson[n].content = childNodes[0].nodeType === 3 ? childNodes[n].nodeValue : "<br>";

                        number = n + 1;
                        if (childNodes.length === 2 && lastChildNode.nodeType === 3) number--;
                        else if (cJson[number] && cJson[number].block === "aside") number++;

                        text = lastChildNode.nodeType === 3 ? lastChildNode.nodeValue : "<br>";
                        block = new Block("p", text, []);
                        cJson.splice(number, 0, block);
                        incraseLength++;
                    }
                }
            }

        } else {
            if (name !== "aside") {
                var flag = true;

                if (name === "div") {
                    var lastChildNodeName = childNode.lastChild.nodeName;
                    var nextSibling = childNode.nextSibling;

                    if (nextSibling && nextSibling.nodeName === "DIV" && (lastChildNodeName === "DFN" || lastChildNodeName === "A")) {
                        cJson[n].content = text;
                        flag = false;
                    }
                }

                if (flag) {
                    changeTextOptionToP();
                    number = n;

                    if (cJson[n].block === "aside") {
                        if (isBlink) {
                            var firstChild = childNode.children[0];
                            text = firstChild.nodeName === "BR" ? "<br>" : firstChild.innerText;
                        }
                        number++;
                    }
                    block = new Block("p", text, []);
                    cJson.splice(number, 0, block);
                    incraseLength++;
                }
            }
        }

    } else {

        if (isTextTyped) {
            if (name === "li") {
                if (currentTextOption.id !== "ul") {
                    changeTextOption(document.getElementById("ul"));
                }
                showPopup("UL");
                cJson.push(new Block(name, text, []));

            } else if (name === "section" && isBlink) {
                var lastChild = childNode.children[0];

                if (n > 0 && lastChild && (lastChild.localName === "a" || lastChild.localName === "dfn")) {

                    changeTextOptionToP();
                    number = cJson[n - 1].content.length;
                    block = new Block("p", text, []);
                    cJson[n - 1].content.splice(number, 0, block);

                }

            } else if (name !== "ul") {
                changeTextOptionToP();

                if (n > 0 && section && cJson[n - 1].content === "<br>") {
                    return true;

                } else {
                    cJson.push(new Block("p", text, []));

                }

            }

        } else {
            var attr = childNode.attributes;
            var modifier = [];

            for (var a = 0; a < attr.length; a++) {
                modifier.push(new Modifier(attr[a].name, attr[a].value));
            }

            if (name === "ul" || name === "section") {
                children = childNode.children;
                var list = [];

                for (child = 0; child < children.length; child++) {
                    list.push(new Block(children[child].localName, children[child].innerHTML, []));
                }
                cJson.push(new Block(name, list, modifier));

            } else {
                cJson.push(new Block(name, text, modifier));
            }
        }
    }

    return incraseLength;
}

function getCurrentBlock(withLI = true, withoutInnerList = false, withoutInnerA = false) {
    var line = countRealLline();
    var len = line.length;
    var obj = json[line[len - 1]];
    var child;

    if (len > 1 && (!withoutInnerList || obj.block !== "ul")) {
        for (var j = 2; j <= len; j++) {
            child = obj.content[line[len - j]];

            if (!withLI && child.block === "li" || typeof child.block === "undefined" || (withoutInnerA && child.block === "a" && obj.block !== "section")) break;
            else if (withoutInnerList && child.block === "ul") {
                obj = child;
                break;

            } else obj = child;
        }
    }
    return obj;
}

function checkIsBlockChanged(isTextTyped) {

    if (isTextTyped) {

        var element = getCurrentBlock(false, true);
        var content = element.content;
        var isLink = false;
        var text = "";

        textOpt = (currentTextOption.id === "header") ? currentHeaderOption.id : currentTextOption.id;

        if (element.block !== "aside" && element.block !== "section" && element.block !== textOpt) {
            if (textOpt === "ul") {
                element.content = [new Block("li", content, [])];

            } else if (textOpt === "section") {
                changeTextOption(document.getElementById("header"));

                if (element.block === "ul") {
                    text = saveContentToString(content);
                    content = text.slice(0, -1);
                }
                element.content = [new Block(currentHeaderOption.id, content, [])];

            } else if (textOpt === "a" && content !== "<br>") {
                var secElement = element;
                isLink = true;

                if (element.block === "ul") {
                    secElement = getCurrentBlock(true, false);
                    content = secElement.content;
                }

                if (secElement.block !== 'a') {
                    if (content !== "<br>") {
                        caretPosition += 6 + countRealLline().length - 1;
                        element = secElement;
                        element.content = [{ "content": element.content }, new Block("a", "link", [])];

                    } else {
                        isLink = false;
                        text = saveContentToString(element.content);
                        element.content = text.slice(0, -1);
                    }
                }

            } else if (element.block === "ul") {
                text = saveContentToString(content);
                element.content = text.slice(0, -1);
            }

            if (!isLink) {
                element.block = textOpt;
                element.modifier = [];
            }


        } else if (textOpt === "ul" && listClicked) {
            listClicked = false;
            element = getCurrentBlock();
            content = element.content;

            block = new Block("li", content, []);
            element.content = [block];
            element.block = "ul";
        }
    }

    displayResult();
    var data = getCaretData();
    setCaretPosition(data);
}

function saveContentToString(content) {
    var text = "";
    var cc;

    for (var c in content) {
        cc = content[c].content;
        if (cc instanceof Array) {
            text += saveContentToString(cc);
        } else text += cc + ' ';
    }
    return text;
}

function textToJson(keyCode = 0) {
    var textOpt = (currentTextOption.id === "header") ? currentHeaderOption.id : currentTextOption.id;
    var doc = new DOMParser().parseFromString(textArea.innerHTML, "text/html");
    var bodyChild = doc.body.children;
    var bodyChildLen = bodyChild.length;
    var text, data;

    var isTextTyped = json.length !== 0 || bodyChildLen - json.length < 2 ? true : false;
    if (bodyChildLen === 2 && bodyChild[0].nodeName === "DIV" && bodyChild[1].nodeName === "DIV") {
        isTextTyped = true;
    }

    if (isTextTyped) {
        saveCusrsorPosition();
    }

    if (json.length === 0 && isTextTyped) {
        text = textArea.innerText;
        text = text === '\n' || text === '\n\n\n' || text === '' ? "<br>" : text;
        text = textOpt === "ul" ? [new Block("li", text, [])] : text;

        if (textOpt === "section") {
            changeTextOption(document.getElementById("header"));
            text = [new Block(currentHeaderOption.id, text, [])];
        }
        json.push(new Block(textOpt, text, []));

    } else {

        for (var n = 0; n < bodyChild.length; n++) {
            var il = processText(bodyChild[n], json, n, isTextTyped);
            if (il === 100) keyCode = 13;
            else if (il === 999 && keyCode !== 13) keyCode = 999;
            bodyChildLen += il;
        }

        if (json.length > bodyChildLen) {
            while (bodyChildLen < json.length) {
                json.pop();
            }
        }
    }


    if (keyCode === 8 || keyCode === 13 || keyCode === 999) {
        displayResult();
        closeLinkMenu();
        closeHeaderMenu();

        if (keyCode === 13) {
            setPositionToNextNode();

        } else {
            if (keyCode === 8) {
                checkIsTextOptionChanged();
            }
            data = getCaretData();
            setCaretPosition(data);
        }
    } else {
        checkIsBlockChanged(isTextTyped);
    }
}

function setPositionToNextNode() {
    var data = getCaretData();
    var nodes = getTextNodes();

    for (var n = 0; n < nodes.length; n++) {
        if (nodes[n] === data.node) {
            if (nodes[n + 1] && nodes[n + 1].parentNode.nodeName === "ASIDE" && nodes[n + 2]) {
                data = { node: nodes[n + 2], line: data.line + 2, position: 0 };

            } else if (nodes[n + 1]) {
                data = { node: nodes[n + 1], line: data.line + 1, position: 0 };
            }
            setCaretPosition(data);
            break;
        }
    }
}

function saveCusrsorPosition() {
    var pos = getCaretPosition();

    if (pos === 0) {
        pos = caretPosition;
    }
    caretPosition = pos;
}

function getTextNodes() {
    var walker = document.createTreeWalker(textArea, NodeFilter.SHOW_ALL, null, false);
    var nodes = [];
    var textNodes = [];
    var lastElementPos = 0;
    var n;

    while (n = walker.nextNode()) {
        if (!(n.nodeName === "#text" && n.nodeValue[0] === '\n') && n.nodeName !== "UL" && n.nodeName !== "SECTION") {
            n.lastElementPos = lastElementPos;
            lastElementPos = 0;
            nodes.push(n);

        } else if (n.nodeName === "#text" && n.nodeValue[0] === '\n') {
            lastElementPos += n.length;
        }
    }

    n = 0;
    while (n < nodes.length) {
        lastElementPos = nodes[n].lastElementPos;

        if (nodes[n].nodeName === "#text") {
            if (!isBlink || nodes[n].nodeValue.length !== 1)
            textNodes.push(nodes[n]);
            n += 1;

        } else if (nodes[n + 1]) {
            if (nodes[n + 1].nodeName === "BR") {
                textNodes.push(nodes[n]);

            } else {
                nodes[n + 1].lastElementPos = lastElementPos;
                textNodes.push(nodes[n + 1]);
            }
            n += 2;
        } else break;
    }
    return textNodes;
}

function getCaretData() {
    var nodes = getTextNodes();
    var position = caretPosition;
    var node, value, nodeVal, lastElementPos;

    for (var n = 0; n < nodes.length; n++) {
        nodeVal = nodes[n].nodeValue;
        value = nodeVal ? nodeVal.length : 0;
        lastElementPos = nodes[n].lastElementPos;

        position -= position > lastElementPos ? lastElementPos : position;

        if (position > value && nodes[n + 1]) {
            position -= value;

        } else {
            node = nodes[n];
            break;
        }
    }
    return { node: node, line: n, position: position };
}

function setCaretPosition(data) {
    var sel = window.getSelection();
    var range = document.createRange();

    if (data.node && data.position > -1) {
        var position = data.position;
        var nodeLen = data.node.length;

        nodeLen = nodeLen ? nodeLen : 1;
        position = nodeLen > position ? position : nodeLen;
        range.setStart(data.node, position);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }
}

function getCaretPosition() {
    var caretOffset = 0;

    if (window.getSelection().rangeCount > 0) {
        var range = window.getSelection().getRangeAt(0);
        var preCaretRange = range.cloneRange();

        preCaretRange.selectNodeContents(textArea);
        preCaretRange.setEnd(range.endContainer, range.endOffset);
        caretOffset = preCaretRange.toString().length;
    }
    return caretOffset;
}

function jsonToHtml() {
    var html = "";

    for (var j in json) {
        var block = json[j].block;
        var content = json[j].content;
        var modifier = json[j].modifier;
        if (block === "") break;

        html += '<' + block;
        if (modifier instanceof Array) {
            for (var m in modifier) {
                html += ' ' + modifier[m].name + '="' + modifier[m].value + '"';
            }
        }
        html += '>';

        if (content instanceof Array) {
            html += saveContentToHTML(content) + "\n";

        } else {
            html += content;
        }

        html += '</' + json[j].block + '>\n';
    }
    return html;
}

function saveContentToHTML(content, times = 0) {
    times++;
    var html = "";
    var space = '\n' + '\t'.repeat(times);

    for (var c in content) {
        var block = content[c].block;
        var modifier = content[c].modifier;

        if (typeof block !== "undefined") {

            html += space + "<" + block;
            for (var m in modifier) {
                html += ' ' + modifier[m].name + '="' + modifier[m].value + '"';
            }

            if (content[c].content instanceof Array) {
                html += ">" + saveContentToHTML(content[c].content, times) + space + "</" + block + ">";

            } else {
                html += ">" + content[c].content + "</" + block + ">";
            }
        } else {
            html += content[c].content;
        }
    }
    return html;
}

function replacer(key, value) {
    if (value instanceof Object) {
        if (Object.keys(value).length === 0) {
            return undefined;
        }

    } else if (value === '') {
        return undefined;
    }
    return value;
}

function displayResult() {
    var html = jsonToHtml();
    textArea.innerHTML = html;

    switch (currentDisplayOption.id) {
        case 'json':
            if (json.length > 0) {
                resultArea.innerText = JSON.stringify(json, replacer, 2);
            } else {
                resultArea.innerText = "";
            }
            break;

        case 'html':
            resultArea.innerText = html;
            break;

        default:
            resultArea.innerText = "Błąd wyświetlania";
    }
}